import store from "./store";
import * as actions from "./actionTypes";
import { bugAdded, bugResolved } from "./actions";

const unsubsribe = store.subscribe(() => {
  console.log("Store changed", store.getState());
});

store.dispatch(bugAdded("description"));
store.dispatch(bugResolved(1));
console.log(store.getState());
unsubsribe();
