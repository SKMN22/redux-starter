import { produce } from "immer";

let book = Map({ title: "Harry Potter" });

function publish(book) {
  return produce(book, (drafBook) => {
    drafBook.publish = true;
  });
}

publish(book);
console.log(book);
