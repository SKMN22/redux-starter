import { compose, pipe } from "lodash/fp";

const input = "Example Input";
const toLowerCase = (str) => str.toLowerCase();
const trim = (str) => str.trim();
// Currying -> return function with one parameter
const wrap = (type) => (str) => `<${type}>${str}</${type}>`;

const transform = pipe(trim, toLowerCase, wrap("span"));
console.log(transform(input));
