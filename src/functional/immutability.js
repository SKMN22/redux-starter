const person = {
  name: "Roman",
  address: {
    country: "USA",
    city: "New York",
  },
};
// const newPerson = Object.assign({}, person, { name: "Prd" });
// console.log("newPerson");
// Shallow Copy
const updated = { ...person, name: "Zdeno" };
// Deep Copy
const deepCopy = {
  ...person,
  addres: {
    ...person.address,
  },
};
console.log(updated);

// Adding
const numbers = [1, 2, 3];
const added = [...numbers, 4];
const index = numbers.indexOf(2);
const splitted = [...numbers.slice(0, index), 8, ...numbers(index)];

//Removing

const nerArray = numbers.filter((n) => n !== 2);

// Updating

const updatedArray = numbers.map((n) => (n === 2 ? 20 : n));
console.log(updatedArray);
